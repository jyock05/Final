package com.ccac.acd.jyock05.pizzatime;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.Parse;

public class MainActivity extends AppCompatActivity {

    private AnimationDrawable pizzaZone;
    private ImageView pizza;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //defines the image view
        pizza = (ImageView) findViewById(R.id.pizzaZone);

        //sets the image view to the drawable
        pizza.setBackgroundResource(R.drawable.pizza);

        //begins the repeating cycle for the welcome screen.
        pizzaZone = (AnimationDrawable) pizza.getBackground();
        pizzaZone.start();

        //tells the user what to do  with a little toast
        Toast.makeText(MainActivity.this, "Tap the Pizza to begin!", Toast.LENGTH_LONG).show();

        pizza.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                startActivity(new Intent(MainActivity.this, SecondActivity.class));
                return false;
            }
        });
    }
}
