package com.ccac.acd.jyock05.pizzatime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class FourthActivity extends AppCompatActivity {
    private int size = 0;
    private int pepTop = 0;
    private int sausTop = 0;
    private int bacTop = 0;
    private int extraChesTop = 0;
    private int toppingTotal = 0;
    private double Total = 0;

    private String name = "";
    private String phoneNumber;
    private String sizeConvert;

    private boolean delivBol;

    private String objectId;
    private String details;

    private String message;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fourth_activity);

        // Enable Local Datastore.
    //    Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("he4B0Z9vCuIeIMOCDiurtejFbDcUvyDjsaulfINq")
                .clientKey("kUK4pnECJof1y2UslNSj6S0uABw5hX0o8ziEjO58")
                .server("https://parseapi.back4app.com/")
                .build()
        );

        ParseUser.enableAutomaticUser();

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);


        Button confirm = (Button) findViewById(R.id.btnConfirm);
        Button restart = (Button) findViewById(R.id.btnRestart);

        TextView orderDetails = (TextView) findViewById(R.id.tvDisplay);

        Bundle extras = getIntent().getExtras();

        if (extras != null){

            //gets the previous intent's extras and passes the data for calculation
            size = extras.getInt("size");
            pepTop = extras.getInt("pepperoni");
            sausTop = extras.getInt("sausage");
            extraChesTop = extras.getInt("extra cheese");
            bacTop = extras.getInt("bacon");

            name = extras.getString("name");
            phoneNumber = extras.getString("phone number");
            toppingTotal = extras.getInt("topping total");
            Total = extras.getDouble("total price");
            delivBol = extras.getBoolean("delivery");
        }

        //gives an output in string value for chosen size
        switch (size){
            case 1:
                sizeConvert = "small";
                break;
            case 2:
                sizeConvert = "medium";
                break;
            case 3:
                sizeConvert = "large";
                break;
        }

        //creates string the output for toppings
        StringBuilder sbTopping = new StringBuilder();
        if (pepTop == 1) {
            String topping1 = "Pepperoni\n";
            sbTopping.append(topping1);
        }
        if (sausTop == 1) {
            String topping2 = "Sausage\n";
            sbTopping.append(topping2);
        }
        if (extraChesTop == 1){
            String topping3 = "Extra cheese\n";
            sbTopping.append(topping3);
        }
        if (bacTop == 1){
            String topping4 = "Bacon\n";
            sbTopping.append(topping4);
        }

        if (toppingTotal == 0) {
            String lonelyCheese = "Cheese and cheese alone" +
                    "\nMaybe you shoulda got extra cheese?\n";
            sbTopping.append(lonelyCheese);
        }

        String delivStr = "";
        if (delivBol)
            delivStr = "You will be getting delivery.\n" +
                    "The fee is not a tip to the driver.";
        else
            delivStr = "You opted to pick-up\n"+
                    "See you soon in store!";

        //Output string for textview
        StringBuilder detailOrder = new StringBuilder();
        String doOver = "Otherwise please restart the order process.";
        String correct = "If these details are correct please hit confirm.\n";
        detailOrder.append("Your order details:\n"
                + "Name: " + "                    " + name +"\n"
                + "Phone Number:" +  "     " + phoneNumber + "\n" + "\n"
                + "A " + sizeConvert +  " " + toppingTotal + " topping pizza" + "\n"
                + "With:"+ "\n" + sbTopping + "\n" + "Total: $" + Total + "\n" + "\n"
                + delivStr + "\n"
                + correct + doOver);

        orderDetails.setText(detailOrder);

        //a more accurate version for Parse to be retrieved
        StringBuilder serverDescription = new StringBuilder();
        serverDescription.append("Your order details:\n"
                + "Name: " + "                    " + name +"\n"
                + "Phone Number:" +  "     " + phoneNumber + "\n" + "\n"
                + "A " + sizeConvert +  " " + toppingTotal + " topping pizza" + "\n"
                + "With:"+ "\n" + sbTopping + "\n" + "Total: $" + Total + "\n" + "\n"
                + delivStr);
        details = serverDescription.toString();

        message = new StringBuilder("Thank you for the order.\n" +
                "\nTo check on the status type in the code:\n\n\n"
                + "IT IS CASE SENSITIVE!").toString();

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createObject();
                Intent intent = new Intent(FourthActivity.this, FifthActivity.class);
                intent.putExtra("confirm", objectId);
                intent.putExtra("loadMessage", message);
                startActivity(intent);
            }
        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FourthActivity.this, SecondActivity.class));
            }
        });
    }
        public void createObject()  {
            ParseObject confirmedOrder = new ParseObject("orders");
            confirmedOrder.put("size", size);
            confirmedOrder.put("pepperoni", pepTop);
            confirmedOrder.put("sausage", sausTop);
            confirmedOrder.put("extra_cheese", extraChesTop);
            confirmedOrder.put("bacon", bacTop);
            confirmedOrder.put("name", name);
            confirmedOrder.put("phone", phoneNumber);
            confirmedOrder.put("delivery", delivBol);
            confirmedOrder.put("detailed", details);

            // Saves the new object.
            // Notice that the SaveCallback is totally optional!
            try {
                confirmedOrder.save();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            objectId = confirmedOrder.getObjectId();
            }


    }


