package com.ccac.acd.jyock05.pizzatime;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    private Button back;
    private Button go;
    private Button calc;

    private TextView totVal;

    private EditText nameInput;
    private EditText phoneInput;

    private double Total = 0;

    private double sizePrice = 0;
    private double deliveryPrice = 0;

    private int size = 0;
    private int pepTop = 0;
    private int sausTop = 0;
    private int bacTop = 0;
    private int extraChesTop = 0;
    private int toppingTotal = 0;

    private String name = "";
    private String phoneNumber;

    private Boolean delivBol = false;

    private Boolean nameBol = false;
    private Boolean phoneBol = false;

    private MediaPlayer mediaPlayer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.chaching);

        back = (Button) findViewById(R.id.btnBack);
        go = (Button) findViewById(R.id.btnConfirm);
        calc = (Button) findViewById(R.id.btnCalc);

        nameInput = (EditText) findViewById(R.id.etName);
        phoneInput = (EditText) findViewById(R.id.etPhoneInput);

        totVal = (TextView) findViewById(R.id.tvTotalVal);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            //gets the previous intent's extras and passes the data for calculation
            size = extras.getInt("size");
            pepTop = extras.getInt("pepperoni");
            sausTop = extras.getInt("sausage");
            extraChesTop = extras.getInt("extra cheese");
            bacTop = extras.getInt("bacon");

            //adds all the toppings into one value for calculation
            toppingTotal = pepTop + sausTop + bacTop + extraChesTop;
        }

        //set the size prices and totals for the toppings (varies on size
        switch (size) {
            case 1:
                sizePrice = 10;
                break;
            case 2:
                sizePrice = 12;
                break;
            case 3:
                sizePrice = 16;
                break;
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ThirdActivity.this, SecondActivity.class));

            }
        });

        final MediaPlayer finalMediaPlayer = mediaPlayer;
        calc.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (nameInput.getText().toString().isEmpty()) {
                    // editText is empty
                    Toast.makeText(getBaseContext(), "You must provide your details."
                            , Toast.LENGTH_LONG).show();
                    nameBol = false;
                } else {
                    // editText is not empty
                    nameBol = true;
                }

                if (phoneInput.getText().toString().isEmpty() || phoneInput.getText().toString().length() < 10) {
                    // editText is empty
                    Toast.makeText(getBaseContext(), "You must provide your details."
                            , Toast.LENGTH_LONG).show();
                    phoneBol = false;

                } else {
                    // editText is not empty
                    phoneBol = true;
                }

                //checks to see if both are true before allowing calculation and continuation
                if (nameBol && phoneBol) {
                    name = nameInput.getText().toString();
                    phoneNumber = phoneInput.getText().toString();

                    Total = sizePrice + (toppingTotal * size) + deliveryPrice;
                    totVal.setText("$" + Total);
                    go.setEnabled(true);
                    if (mediaPlayer.isPlaying()) {
                        stopMusic();
                    } else {
                        startMusic();
                    }
                }
            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sends all the old data and now the new data to build a string in the fourth activity
                Intent intent = new Intent(ThirdActivity.this, FourthActivity.class);
                intent.putExtra("size", size);
                intent.putExtra("pepperoni", pepTop);
                intent.putExtra("sausage", sausTop);
                intent.putExtra("extra cheese", extraChesTop);
                intent.putExtra("bacon", bacTop);
                intent.putExtra("name", name);
                intent.putExtra("phone number", phoneNumber);
                intent.putExtra("topping total", toppingTotal);
                intent.putExtra("total price", Total);
                intent.putExtra("delivery", delivBol);

                finish();
                startActivity(intent);
            }
        });
    }

    public void rbClick1(View view) {
        deliveryPrice = 0;
        delivBol = false;
        calc.setEnabled(true);
    }

    public void rbClick2(View view) {
        deliveryPrice = 4;
        delivBol = true;
        calc.setEnabled(true);
        Toast.makeText(getBaseContext(), "There is a $4.00 Delivery Fee." +
                        "\nThis is not a tip."
                , Toast.LENGTH_LONG).show();
    }

    public void stopMusic() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();

        }
    }

    public void startMusic() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    protected void onDestroy() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }
}