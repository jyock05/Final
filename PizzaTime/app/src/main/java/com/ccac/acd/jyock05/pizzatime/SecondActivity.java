package com.ccac.acd.jyock05.pizzatime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


public class SecondActivity extends AppCompatActivity {
    private Button clear;
    private Button submit;
    private Button checkOrder;

    private CheckBox bacon;
    private CheckBox extraCheese;
    private CheckBox pepperoni;
    private CheckBox sausage;

    private int pepTop;
    private int sausTop;
    private int bacTop;
    private int extraChesTop;

    private String topping1;
    private String topping2;
    private String topping3;
    private String message;

    private int size = 0;

    private TextView toppings;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        clear = (Button) findViewById(R.id.btnClear);
        submit = (Button) findViewById(R.id.btnConfirm);
        checkOrder = (Button) findViewById(R.id.btnCheckOrder);

        bacon = (CheckBox) findViewById(R.id.cbBacon);
        extraCheese = (CheckBox) findViewById(R.id.cbExtraCheese);
        sausage = (CheckBox) findViewById(R.id.cbSausage);
        pepperoni = (CheckBox) findViewById(R.id.cbPepperoni);

        toppings = (TextView) findViewById(R.id.tvTopping);

        topping1 = "Toppings are $1.00 Each";
        topping2 = "Toppings are $2.00 Each";
        topping3 = "Toppings are $3.00 Each";

        message = "To check on the status\nType in your code.\n\n\n"
                + "IT IS CASE SENSITIVE!";

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                intent.putExtra("size", size);
                intent.putExtra("pepperoni", pepTop);
                intent.putExtra("sausage", sausTop);
                intent.putExtra("extra cheese", extraChesTop);
                intent.putExtra("bacon", bacTop);
                finish();
                startActivity(intent);
            }
        });

        checkOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, FifthActivity.class);
                intent.putExtra("confirm", "");
                intent.putExtra("loadMessage", message);
                finish();
                startActivity(intent);

                }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pepperoni.setEnabled(false);
                sausage.setEnabled(false);
                extraCheese.setEnabled(false);
                bacon.setEnabled(false);

                submit.setEnabled(false);
                clear.setEnabled(false);

                size = 0;
                toppings.setText("");
            }
        });
    }

    public void rbClick1(View view){
        //enables the rest of the screen to continue
        pepperoni.setEnabled(true);
        sausage.setEnabled(true);
        extraCheese.setEnabled(true);
        bacon.setEnabled(true);

        submit.setEnabled(true);
        clear.setEnabled(true);

        size = 1;
        toppings.setText(topping1);
    }

    public void rbClick2(View view){
        //enables the rest of the screen to continue
        pepperoni.setEnabled(true);
        sausage.setEnabled(true);
        extraCheese.setEnabled(true);
        bacon.setEnabled(true);

        submit.setEnabled(true);
        clear.setEnabled(true);

        size = 2;
        toppings.setText(topping2);
    }

    public void rbClick3(View view){
        //enables the rest of the screen to continue
        pepperoni.setEnabled(true);
        sausage.setEnabled(true);
        extraCheese.setEnabled(true);
        bacon.setEnabled(true);

        submit.setEnabled(true);
        clear.setEnabled(true);

        size = 3;
        toppings.setText(topping3);
    }

    public void cbClick1(View view){
        if (pepperoni.isChecked())
            pepTop = 1;
        else
            pepTop = 0;
    }

    public void cbClick2(View view){
        if (sausage.isChecked())
            sausTop = 1;
        else
            sausTop = 0;
    }

    public void cbClick3(View view){
        if (extraCheese.isChecked())
            extraChesTop = 1;
        else
            extraChesTop = 0;
    }

    public void cbClick4(View view){
        if (bacon.isChecked())
            bacTop = 1;
        else
            bacTop = 0;
    }

}
