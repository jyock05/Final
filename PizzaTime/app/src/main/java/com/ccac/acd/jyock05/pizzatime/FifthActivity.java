package com.ccac.acd.jyock05.pizzatime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseACL;
import com.parse.ParseUser;


public class FifthActivity extends AppCompatActivity {
    private Button orderSearch;
    private Button startOver;
    private EditText parseSearch;

    private TextView outputText;
    private String confirmOrder;

    private String loadMessage;
    private String searchError;


    private String details;

    private String userInput;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fifth_activity);

        // Add your initialization code here
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("he4B0Z9vCuIeIMOCDiurtejFbDcUvyDjsaulfINq")
                .clientKey("kUK4pnECJof1y2UslNSj6S0uABw5hX0o8ziEjO58")
                .server("https://parseapi.back4app.com/")
                .build()
        );

        ParseUser.enableAutomaticUser();

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        searchError = "The order# you used is not valid.\n" +
                "Please ensure you entered the\ncorrectly" +
                "\nit is CASE-SENSITIVE";

        orderSearch = (Button) findViewById(R.id.btnSearch);
        startOver = (Button) findViewById(R.id.btnNewOrder);

        outputText = (TextView) findViewById(R.id.tvOrderOut);

        parseSearch = (EditText) findViewById(R.id.etParseInput);

        //brings the Parse ID here to be searched.
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            //gets the previous intent's extras and passes the data for calculation
            loadMessage = extras.getString("loadMessage");
            confirmOrder = extras.getString("confirm");
        }

        //sets the text to show the parse id and the message

        outputText.setText(loadMessage + "\n" + confirmOrder);

        orderSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // editText is not empty
                if(parseSearch.getText().toString().isEmpty() || parseSearch.getText().toString().length() < 10) {
                    // editText is empty
                    Toast.makeText(getBaseContext(),"You must provide your confirmation code."
                            ,Toast.LENGTH_LONG).show();
                } else {
                    userInput = parseSearch.getText().toString();
                    readObject();
                    outputText.setText(details);
                }
            }

        });

        startOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FifthActivity.this, SecondActivity.class));
            }
        });
    }
    public void readObject() {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("orders");
                    // The query will search for a ParseObject, given its objectId.
                    // When the query finishes running, it will invoke the GetCallback
                    // with either the object, or the exception thrown
                    query.getInBackground(userInput, new GetCallback<ParseObject>() {
                        public void done(ParseObject result, ParseException e) {
                            if (e == null) {
                                details = result.getString("detailed");
                            } else {
                                // something went wrong
                                Toast.makeText(getBaseContext(),"No valid orders found" +
                                                "\nOrders are CASE-SENSTIVE",Toast.LENGTH_LONG).show();
                                outputText.setText(searchError);
                            }
                        }
                    });
    }
}
